import cv2
import numpy as np
import matplotlib.pyplot as plt

def canny(img):
    # converting to gray scale --> faster processing
    gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

    # reduce noise --> gaussian blur --> use kernel which sets each pixel to the average value of its neighbors, smoothens it out
    blur_img = cv2.GaussianBlur(gray_img, (7,7), 0)

    # Canny Edge Detector --> identifying any sharp changes in color or intensity --> get gradients in all directions of image, big val big change, small val small change
    # strongest gradents are then traced
    canny_img = cv2.Canny(blur_img, 50, 150)
    return canny_img

def region_of_interest(img):
    #Only look at a specific region creating a traingular mask to only focus on a specific area
    height, width = img.shape
    height -= 50
    width -= 50
    ## USING TRIANGLES
    # Polygons = np.array([
    #     [(25,height), (width,height), (width//2, height//2)],
    #     # [((3/8)*width,0), ((5/8)*width,0), ((3/8)*width,height), ((5/8)*width,height)]
    #     # [(int((3/8)*width), height), (int((5/8)*width), height), (int((1/2)*width),int((3/4)*height)+20)]
    #     # [(0,height), (width,height), (width//2, height-100)]
    #     ])

    ## USING RECTANGLES
    Polygons = np.array([
        [(width, height), (50,height), (int((3/8)*width), int((3/4)*height)),(int((5/8)*width), int((3/4)*height))]
            #(width, height-100), (width,height//2), (0,height//2), (0,height-100)]
        # [([(int((3/8)*width), height), (int((5/8)*width), height), (int((1/2)*width),int((3/4)*height))])],
        # []
    ])
    mask = np.zeros_like(img)
    # filling mask 
    cv2.fillPoly(mask, Polygons, 255) #color white
    #applying mask on image using biwise &
    #part outside of mask will have values of 0 aka black everything in mask stays as is
    mask_img = cv2.bitwise_and(img, mask)
    return mask_img

def dispay_lines(img, lines):
    line_img = np.zeros_like(img)
    if lines is not None:
        for x1,y1,x2,y2 in lines:
            cv2.line(line_img, (x1,y1), (x2,y2), (0,255,0), 30)
    return line_img

def get_cords(img, line_slope_int):
    slope, intercept = line_slope_int
    y1 = img.shape[0]
    y2 = int(y1 * (4/5)) # we only want the line to go 4 fifths of the way up
    x1 = int((y1 - intercept) / slope)
    x2 = int((y2 - intercept) / slope)
    print(img.shape)
    height, width, _ = img.shape
    if x1 > width or x1 < 0 or x2 > width or x2 < 0 or y1 > height or y1 < 0 or y2 > height or y2 < 0:
        return np.array([0,0,0,0])
    return np.array([x1,y1,x2,y2])

def average_slope_intercept(img, lines):
    left_fit = []
    right_fit = []
    if lines is None:
        return (np.array([0,0,0,0]), np.array([0,0,0,0]))
    for line in lines:
        x1, y1, x2, y2 = line.reshape(4)
        slope, intercept = np.polyfit((x1,x2), (y1,y2), 1)
        print(slope, intercept)
        #left lines negative left else right
        if slope < 0:
            left_fit.append((slope, intercept))
        else:
            right_fit.append((slope, intercept))
    
    if left_fit:
        left_fit_avg = np.average(left_fit, axis=0)
        left_line = get_cords(img, left_fit_avg)
    else:
        left_line = np.array([0,0,0,0])
    if right_fit:
        right_fit_avg = np.average(right_fit, axis=0)
        right_line = get_cords(img, right_fit_avg)
    else:
        right_line = np.array([0,0,0,0])

    return np.array([left_line, right_line])

VID_PATH = "vid2.mp4"
cap = cv2.VideoCapture(VID_PATH)
while (cap.isOpened()):
    _, frame = cap.read()
    canny_img = canny(frame)
    masked_img = region_of_interest(canny_img)

    # Finding straight lines and therefore the lane lines --> Hough transform
    lines = cv2.HoughLinesP(masked_img, 1, 5*(np.pi/180), 100, np.array([]), minLineLength=10, maxLineGap=400)
    avg_lines = average_slope_intercept(frame, lines)
    print(avg_lines)
    line_img = dispay_lines(frame,avg_lines)
    full_img = cv2.addWeighted(frame, 0.8, line_img, 1, 0)

    cv2.imshow("full", full_img)
    cv2.imshow("contoured", masked_img)
    if cv2.waitKey(1) == ord('q'):
        break
    
cap.release()
cv2.destroyAllWindows()