# Lane Detection [Python3 OpenCV]

1. Pre process images
2. Find edges
3. Restrict scope of image and only search certain area
4. Find hough lines
5. Segregate lines into left and right lanes and get an average line for each side
6. Draw lines on image and display